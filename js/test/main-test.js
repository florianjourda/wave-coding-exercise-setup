const expect = require('chai').expect
const main = require('../src/main')

describe('Fake function', function() {
  it('returns 1', function() {
    expect(main.fake_function()).to.equal(1)
  })
})
