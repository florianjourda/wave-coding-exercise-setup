"""Main file for coding exercise."""


def fake_method():
    return 1


# Execute only if run as a script
if __name__ == '__main__':
    fake_method()